"""código de publicación
precio
ubicación geográfica (1-23 identificando cada provincia de la Argentina)
estado (nuevo/usado)
cantidad disponible
puntuación del vendedor ( 1-5 identificando 5 como Excelente y 1 Mala)."""

provincias = ["Buenos Aires", "Catamarca", "Chaco", "Chubut", "Córdoba", "Corrientes", "Entre Ríos", "Formosa",
                "Jujuy", "La Pampa", "La Rioja", "Mendoza", "Misiones", "Neuquén", "Río Negro", "Salta", "San Juan",
                "San Luis", "Santa Cruz", "Santa Fe", "Santiago del Estero",
                "Tierra del Fuego, Antártida e Isla del Atlántico Sur", "Tucumán"]
puntuaciones = ["Mala", "Regular", "Buena", "Muy buena", "Excelente"]
estados = ["Nuevo", "Usado"]

class Producto:
    def __init__(self, codigo_publicacion, precio=0, ubicacion=0, estado=0, cant_disponible=0, puntuacion=1):
        self.codigo_publicacion = codigo_publicacion
        self.precio = precio
        if ubicacion < 1 or ubicacion > len(provincias)+1:
            ubicacion = 1
        self.ubicacion = ubicacion
        if estado not in estados:
            estado = estados[0]
        self.estado = estado
        self.cant_disponible = cant_disponible
        if puntuacion < 1 or puntuacion > len(puntuaciones)+1:
            puntuacion = 1
        self.puntuacion = puntuacion

    def write_product(self):
        print("{}\nCódigo de publicación: {}.\nPrecio: {}$.\nUbicacion: {}.\nEstado: {}.\nCantidad disponible: {}.\nPuntuacion: {}.\n{}".format(
            "*"*80, self.codigo_publicacion, self.precio, provincias[self.ubicacion-1],
            self.estado, self.cant_disponible, puntuaciones[self.puntuacion-1], "*"*80
        ))